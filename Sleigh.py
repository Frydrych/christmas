class Sleigh:
    def __init__(self, id, gifts, elves):
        self.id = id
        self.gifts = gifts
        self.elves = elves

    def __str__(self):
        return str(self.id) + ";" + str(self.gifts) + ";" + str(self.elves)