class Gift:
    def __init__(self, id, name, difficulty):
        self.id = id
        self.name = name
        self.difficulty = difficulty

    def __str__(self):
        return str(self.id) + ";" + self.name + ";" + str(self.difficulty)